(function($) {
    'use strict';
    $(function() {
        jQuery.validator.addMethod('percentage', function (value, element) {
            if (this.optional(element))
                return true;

            return /^((0|[1-9]\d?)(\.\d{1,2})?|100(\.00?)?)$/.test(value);
        }, 'Please enter a valid percentage.');
    });
})(jQuery);