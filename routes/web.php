<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::group(['namespace' => 'Product'], function () {
    Route::get('products', 'ProductController@list')->name('products.index');
    Route::get('products/list-ajax', 'ProductController@listAjax')->name('products.list');
    Route::any('products/{encodedId}/edit', 'ProductController@edit')->name('products.edit');
    Route::get('product/add', 'ProductController@create')->name('product.add');
    Route::post('product/insert', 'ProductController@insertProduct')->name('product.store');
});