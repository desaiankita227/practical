<?php

namespace App\Http\Controllers\Product;

use App\Http\Controllers\Controller;
use App\Models\Customer;
use App\Models\UserProduct;
use App\Rules\NumericWithTwoDecimal;
use App\Rules\Percentage;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Validator;

class ProductController extends Controller
{
    /**
     * [index Display a listing of the resource.]
     *
     * @return \Illuminate\View\View
     */
    function list() {
        return view('product.list');
    }

    /**
     * [listAjax List Product]
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function listAjax(Request $request)
    {

        ## Read value
        $draw = $request->get('draw');
        $start = $request->get("start");
        $rowperpage = $request->get("length"); // Rows display per page

        $columnIndex_arr = $request->get('order');
        $columnName_arr = $request->get('columns');
        $order_arr = $request->get('order');
        $search_arr = $request->get('search');

        $columnIndex = $columnIndex_arr[0]['column']; // Column index
        $columnName = $columnName_arr[$columnIndex]['data']; // Column name
        $columnSortOrder = $order_arr[0]['dir']; // asc or desc
        $searchValue = $search_arr['value']; // Search value

        // Total records
        $totalRecords = Customer::select('count(*) as allcount')->count();
        $totalRecordswithFilter = Customer::select('count(*) as allcount')->where('customer_name', 'like', '%' . $searchValue . '%')->count();

        // Fetch records
        $records = Customer::orderBy($columnName, $columnSortOrder)
            ->where('customers.customer_name', 'like', '%' . $searchValue . '%')
            ->select('customers.*')
            ->skip($start)
            ->take($rowperpage)
            ->get();

        $data_arr = array();

        foreach ($records as $record) {
            $id = $record->id;
            $customer_name = $record->customer_name;
            $customer_email = $record->customer_email;
            $edit = route('products.edit', $id);
            $data_arr[] = array(
                "id" => $id,
                "customer_name" => $customer_name,
                "customer_email" => $customer_email,
                "action" => "<a href='{$edit}' title='Edit Product' >Edit Product</a>",
            );
        }

        $response = array(
            "draw" => intval($draw),
            "iTotalRecords" => $totalRecords,
            "iTotalDisplayRecords" => $totalRecordswithFilter,
            "aaData" => $data_arr,
        );

        return response()->json($response);

    }

    /**
     * [index Display a listing of the resource.]
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return $this->_update();
    }

    /**
     * [update Update controller]
     *
     * @param string $id
     * @return void
     */
    public function edit($id)
    {
        $customer = Customer::where('id', $id)->first();
        
        $products = UserProduct::where('customer_id', $id)->get();
        if ($customer === null) {
            return redirect()->route('products.index')->with('error', 'Product not exist');
        }
        return $this->_update($customer, $products);
    }

    /**
     * [_update Create / Update controller]
     *
     * @param object|null $offer Offer object or null
     * @return \Illuminate\View\View
     */
    private function _update($customer = null, $products = null)
    {
        if ($customer == null) {
            $customer = new Customer();
        }
        return view('product.add', compact('customer', 'products'));
    }

    public function insertProduct(Request $request)
    {
         // Rule Validation - Start
        $rule = [
            'customer_name' => ['required', 'max:150'],
            'customer_email' => ["required", "email", Rule::unique("customers", "customer_email")->ignore($request->id), "max:100"],
            "product_name" => "required|array",
            "product_name.*" => "required|string",
            "price" => "required|array",
            "price.*" => ['required', new NumericWithTwoDecimal],
            "discount" => "required|array",
            "discount.*" => ['required', 'max:50', new Percentage],
        ];

        $validator = Validator::make($request->all(), $rule);

        if ($validator->fails()) {
            return back()->withInput($request->all())->withErrors($validator);
        }
        // Rule Validation - End

        // Get customer Detail
        $customer = Customer::find($request->id);


        $postData = $request->only('customer_name', 'customer_email');
        if (isset($request->id) && $request->id > 0) {
            $customer->update($postData);
        }
        else{
            $customer = new Customer($postData);
            $customer->save();
        }
        $position = $request->product_name;

        for ($count = 0; $count < count($position); $count++) {
            $insert = array(
                'customer_id' => $customer->id,
                'product_name' => $request->product_name[$count],
                'product_price' => $request->price[$count],
                'discount' => $request->discount[$count],
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            );
            $product_data[] = $insert;
        }

        UserProduct::insert($product_data);

        return response()->json([
            'success' => true,
            'message' => 'Data inserted successfully',
        ]);

    }
}
