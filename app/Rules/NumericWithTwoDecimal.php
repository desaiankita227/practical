<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class NumericWithTwoDecimal implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return !empty($value) && preg_match("/^\s*(?=.*[1-9])\d*(?:\.\d{1,2})?\s*$/", $value);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Value must be positive numeric with max two decimal points.';
    }
}
