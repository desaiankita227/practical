<!DOCTYPE html>
<html>
    <head>
        <title>Product Listing</title>

        <!-- Meta -->
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta charset="utf-8">

        <!-- Datatables CSS CDN -->
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css">

        <!-- jQuery CDN -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

        <!-- Datatables JS CDN -->
        <script type="text/javascript" src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>

    </head>
    <body>
        <div>
        <a href="{{ route('product.add') }}" class="btn btn-success btn-sm pull-right">  Add Product</a>
        </div>
        <table id='listProduct' width='100%' border="1" style='border-collapse: collapse;'>
            <thead>
                <tr>
                    <td>Customer Name</td>
                    <td>Customer Email</td>
                    <td>Action</td>
                </tr>
            </thead>
        </table>

        <!-- Script -->
        <script type="text/javascript">
        $(document).ready(function(){
            // DataTable
            $('#listProduct').DataTable({
                processing: true,
                serverSide: true,
                ajax: "{{route('products.list')}}",
                columns: [
                    { data: 'customer_name' },
                    { data: 'customer_email' },
                    { data: 'action' },
                ]
            });
        });
        </script>
  </body>
</html>
