<!DOCTYPE html>
<html>
<head>
    <title>Product</title>
    <script src = "https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script src="https://code.jquery.com/jquery-1.11.1.min.js"></script>
	<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
	<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<!-- Font-->
	<link rel="stylesheet" type="text/css" href="{{asset('css/opensans-font.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('fonts/line-awesome/css/line-awesome.min.css')}}">
	<!-- Main Style Css -->
    <link rel="stylesheet" href="{{asset('css/style.css')}}"/>
    <meta name="csrf-token" content="{{ csrf_token() }}" />
</head>

<body class="form-v4">
    <div class="page-content">
		<div class="form-v4-content">
            <form class="form-detail" action="" method="post" id="product" name="product">

                @csrf
                @php $id = ($customer->id > 0 && isset($customer)) ? $customer['id'] : 0; @endphp
                <input type="hidden" name="id" value="{{$id}}">

				<div class="form-row">
                    <label for="customer_name">Customer Name<span style="color:red">&nbsp;*</span></label>
                    <input type="text" name="customer_name" id="customer_name" class="input-text" value="{{ old('customer_name') ? old('customer_name') : $customer['customer_name'] }}">
                    @include('field', ['field' => 'customer_name'])
                </div>

				<div class="form-row">
					<label for="customer_email">Customer Email</label><span style="color:red">&nbsp;*</span>
					<input type="text" name="customer_email" id="customer_email" class="input-text" value="{{ old('customer_email') ? old('customer_email') : $customer['customer_email'] }}">
                    @include('field', ['field' => 'customer_email'])
				</div>

                <div class="form-row">
                    <label for="customer_email">Product</label><span style="color:red">&nbsp;*</span>
                    <button class="add_field_button">Add More Fields</button>
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped table-highlight">
                            <thead>
                                <th>Product Name</th>
                                <th>Price</th>
                                <th>Discount (%)</th>
                            </thead>
                            @if($id > 0)
                                <tbody>
                                    @foreach($products as $product)
                                    <tr>
                                        <td><input type="text" name="product_name" class="form-control" value="{{ $product['product_name'] }}" /></td>
                                        <td><input type="text" name="price" class="form-control" value="{{ $product['product_price'] }}"/></td>
                                        <td><input type="text" name="discount" class="form-control" value="{{ $product['discount'] }}"/></td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            @else
                                <tbody class="input_fields_wrap">
                                    <tr>
                                        <td><input type="text" name="product_name" class="form-control"  /></td>
                                        <td><input type="text" name="price" class="form-control"  onchange="calculateAmount(this.value)"/></td>
                                        <td><input type="text" name="discount" class="form-control"  onchange="calculateDiscount(this.value)"/></td>
                                    </tr>
                                </tbody>
                           @endif
                        </table>
                    </div>
                </div>
                <input name="tot_item" id="tot_item" type="text" readonly>
                <input name="tot_amount" id="tot_amount" type="text" readonly>
                <input name="tot_discount" id="tot_discount" type="text" readonly>
                <input name="tot_bill" id="tot_bill" type="text" readonly>

				<div class="form-row">
					<input type="submit" name="submit" class="btn-submit" value="Save">
				</div>
			</form>
        </div>
    </div>
</body>

<script src="{{ asset('theme/js/custom-validate-methods.js') }}"></script>
<script src="{{ asset('theme/js/numeric-input.js') }}"></script>

<script type="text/javascript">

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    function calculateAmount(val) {
        var fields = document.getElementsByName("price");
        var price = [];
        for(var i = 0; i < fields.length; i++) {
            price.push(fields[i].value);
        };

        var sum = price.reduce((a, b) => a + b, 0);
        var divobj = document.getElementById('tot_amount');
        divobj.value = sum;
    }

    function calculateDiscount(val) {
        var fields = document.getElementsByName("discount");
        var discount = [];
        for(var i = 0; i < fields.length; i++) {
            discount.push(fields[i].value);
        };

        var sum = discount.reduce((a, b) => a + b, 0);
        var divobj = document.getElementById('tot_discount');
        divobj.value = sum;
    }

    $(".btn-submit").click(function(e){

        e.preventDefault();

        var id = '<?php echo $id; ?>';
        var customer_name = $("input[name=customer_name]").val();
        var customer_email = $("input[name=customer_email]").val();
        var product_name = [];
        var fields = document.getElementsByName("product_name");
        for(var i = 0; i < fields.length; i++) {
            product_name.push(fields[i].value);
        };

        var price = [];
        var fields = document.getElementsByName("price");
        for(var i = 0; i < fields.length; i++) {
            price.push(fields[i].value);
        };

        var discount = [];
        var fields = document.getElementsByName("discount");
        for(var i = 0; i < fields.length; i++) {
            discount.push(fields[i].value);
        };
        $.ajax({
            url:"{{ route('product.store') }}",
            method:'POST',
            data:{
                id:id,
                customer_name:customer_name,
                customer_email:customer_email,
                product_name:product_name,
                price:price,
                discount:discount,
            },
            success:function(response){
                if(response.success){
                    alert(response.message) //Message come from controller
                    window.location.href = "{{url('/products')}}";
                }else{
                    alert("Please fill all field with valid value");
                }
            },
            error:function(error){
                console.log(error)
            }
        });
	});

    $(document).ready(function() {
        var add_button = $(".add_field_button"); //Add button ID
        var wrapper = $(".input_fields_wrap"); //Fields wrapper

        $(add_button).click(function(e){
            e.preventDefault();
            $(wrapper).append('<tr><td><input type="text" name="product_name" class="form-control"  /></td><td><input type="text" name="price" class="form-control" onchange="calculateAmount(this.value)"/></td><td><input type="text" name="discount" class="form-control"  onchange="calculateDiscount(this.value)"/></td></tr>'); //add input box
        });

        $(wrapper).on('click', '.remove_button', function(e){
            e.preventDefault();
            $(this).parent('tr').remove(); //Remove field html
            x--; //Decrement field counter
        });


    });
</script>
</html>
